#PHP 7.x 
#FROM registry.gitlab.com/huezo/apache2:ssl-apache2php-focal
#PHP 8+
#FROM registry.gitlab.com/huezo/apache2:apache2-php
FROM drupal:7.36

# File Author / Maintainer
MAINTAINER vhuezo <vhuezo>

WORKDIR /var/www/html/



RUN apt update 
#RUN apt install -y php-apcu php-gd php-mbstring php-uploadprogress php-xml php-mysql php-pgsql php-pdo-sqlite

#RUN rm /var/www/html/index.html

COPY . /var/www/html/
COPY ./sites/default/default.settings.php   /var/www/html/sites/default/settings.php


RUN chown www-data -R /var/www/html/sites 
